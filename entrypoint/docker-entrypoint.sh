#!/bin/sh
echo 'Esperando por conectividade com o banco de dados...'
/wait-for-it.sh -t 0  $PENTAHO_DATABASE_HOST:$PENTAHO_DATABASE_PORT --strict  -- echo "Foi verificado que o banco de dados está disponível"

#rm -rf $PENTAHO_HOME/tomcat/temp
#rm -rf $PENTAHO_HOME/tomcat/work
#rm -rf $PENTAHO_HOME/pentaho-solutions/system/jackrabbit/repository/repository
#rm -rf $PENTAHO_HOME/pentaho-solutions/system/jackrabbit/repository/workspaces

echo 'Configurando variável de ambiente em repository.xml'
FILE_TO_PARSE=$PENTAHO_HOME/pentaho-solutions/system/jackrabbit/repository.xml
envsubst '$HOSTNAME' < $FILE_TO_PARSE > out.txt && mv out.txt $FILE_TO_PARSE

echo 'Configurando variável de ambiente em context.xml'
FILE_TO_PARSE=$PENTAHO_HOME/tomcat/webapps/pentaho/META-INF/context.xml
envsubst '$PENTAHO_DATABASE_HOST,$PENTAHO_DATABASE_PORT' < $FILE_TO_PARSE > out.txt && mv out.txt $FILE_TO_PARSE

#CATALINA_OPTS="-Xms2048m -Xmx6144m -Dsun.rmi.dgc.client.gcInterval=3600000 -Dsun.rmi.dgc.server.gcInterval=3600000 -Dfile.encoding=utf8 -DDI_HOME=\"$DI_HOME\""
export CATALINA_OPTS="$CATALINA_OPTS -DNODE_NAME=$(hostname)"

# Here we wil override all the files from the "/docker-entrypoint-init" into the pentaho base folder.
# This allows users to change configuration files before server starts

echo found $(find /docker-entrypoint-init/ -type f -print | wc -l) files to be copied
cp -r /docker-entrypoint-init/* /opt/pentaho/pentaho-server/

./tomcat/bin/catalina.sh run
#exec "$@"
#/bin/sh