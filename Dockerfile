FROM redhat/ubi8:8.5-226.1645809065

COPY htop-3.0.5-1.el8.x86_64.rpm /tmp
RUN yum localinstall -y /tmp/htop-3.0.5-1.el8.x86_64.rpm
RUN rm /tmp/htop-3.0.5-1.el8.x86_64.rpm

#COPY postgresql-42.3.3.jar $PENTAHO_HOME/tomcat/lib
#ENV PENTAHO_USER_HOME "/home/pentaho"
ENV JAVA_HOME "/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.322.b06-2.el8_5.x86_64/"
ENV PENTAHO_HOME "/opt/pentaho/pentaho-server"
ENV PENTAHO_JAVA_HOME $JAVA_HOME
ENV PENTAHO_USER=root

RUN yum update -y; yum install -y unzip lsof nano java-1.8.0-openjdk-devel libXtst.x86_64 libXi.x86_64 libXrender.x86_64 hostname iputils gettext

RUN mkdir -p /opt/pentaho
COPY pentaho-server-ce-9.2.0.0-290.zip /tmp/pdi.zip
RUN  unzip -q /tmp/pdi.zip -d /opt/pentaho/
RUN  rm /tmp/pdi.zip

COPY ./init/pentaho/quartz.properties  /opt/pentaho/pentaho-server/pentaho-solutions/system/quartz/quartz.properties
COPY ./init/pentaho/hibernate-settings.xml /opt/pentaho/pentaho-server/pentaho-solutions/system/hibernate/hibernate-settings.xml
COPY ./init/pentaho/repository.xml /opt/pentaho/pentaho-server/pentaho-solutions/system/jackrabbit/repository.xml
COPY ./init/pentaho/context.xml /opt/pentaho/pentaho-server/tomcat/webapps/pentaho/META-INF/context.xml  

COPY ./entrypoint/ /       

#Use pentaho user
#USER ${PENTAHO_USER}
WORKDIR ${PENTAHO_HOME}
ENTRYPOINT ["/docker-entrypoint.sh"]
EXPOSE 8080
#CMD [ "sh", "./tomcat/bin/catalina.sh", "start" ]