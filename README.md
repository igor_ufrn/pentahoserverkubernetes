Exemplo de como disponibilizar uma aplicação através do Kubernetes,
definindo:

- configMaps.yml
- secrets.yml
- db-manager.yml
- nginx-ingress.yml: sticky session
- pentahoKubernetes.yml
- postgres-deployment.yml
- volumes.yml

No caso foi usada como exemplo a aplicação PentahoServer Web (Dockerfile).
